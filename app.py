import csv
import time
import datetime

from flask import Flask, g, jsonify, request

app = Flask(__name__)


@app.route("/")
def hello():
    return "Hello World!"


@app.route("/search")
def search():
    word = request.args.get('word')
    if not word:
        return jsonify([])

    results = search_words(word.lower())

    return jsonify([result[0] for result in results])


def search_words(word):
    with open('word_search.tsv', "r") as tsvfile:
        tsvreader = csv.reader(tsvfile, delimiter='\t')

        match = []
        containslist = []
        results = []

        for row in tsvreader:
            if row[0] == word:
                match = [(row[0], 0, 0)]
            elif word in row[0] and len(word) >= 2:
                containslist.append((row[0], row[0].find(word), int(row[1])))
            elif len(row[0]) >= len(word) - 2 and len(row[0]) <= len(word) + 2 and len(word) > 3:
                if len(row[0]) > len(word):
                    ldistance = levenshtein_distance(word, row[0])
                else:
                    ldistance = levenshtein_distance(row[0], word)

                if ldistance:
                    results.append((row[0], ldistance, int(row[1])))

            if len(containslist) == 25:
                break

        containslist.sort(key=lambda x: x[2], reverse=True)
        containslist.sort(key=lambda x: len(x[0]))
        containslist.sort(key=lambda x: x[1])

        results.sort(key=lambda x: x[2], reverse=True)
        results.sort(key=lambda x: len(x[0]))
        results.sort(key=lambda x: x[1])

        results = match + containslist + results

        return results[:25]


def levenshtein_distance(worda, wordb):
    v0 = [None] * (len(wordb) + 1)
    v1 = [None] * (len(wordb) + 1)
    for i in range(len(v0)):
        v0[i] = i
    for i in range(len(worda)):
        v1[0] = i + 1
        for j in range(len(wordb)):
            cost = 0 if worda[i] == wordb[j] else 1
            v1[j + 1] = min(v1[j] + 1, v0[j + 1] + 1, v0[j] + cost)
        for j in range(len(v0)):
            v0[j] = v1[j]

    return v1[len(wordb)]


@app.before_request
def start_timer():
    g.start = time.time()


@app.after_request
def log_request(response):
    if request.path == '/favicon.ico':
        return response
    elif request.path.startswith('/static'):
        return response

    now = time.time()
    duration = round(now - g.start, 2)
    timestamp = datetime.datetime.fromtimestamp(now).strftime('%Y-%m-%d %H:%M:%S')
    ip = request.headers.get('X-Forwarded-For', request.remote_addr)
    host = request.host.split(':', 1)[0]
    args = dict(request.args)

    log_params = [
        ('method', request.method),
        ('path', request.path),
        ('status', response.status_code),
        ('duration', duration),
        ('time', timestamp),
        ('ip', ip),
        ('host', host),
        ('params', args)
    ]

    parts = []
    for name, value in log_params:
        part = "{}={}".format(name, value)
        parts.append(part)
    line = " ".join(parts)

    app.logger.info(line)

    return response


if __name__ == "__main__":
    app.run(use_debugger=True, use_reloader=True)
